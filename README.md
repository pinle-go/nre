# code for NRE

To reproduce the results, **Python 3.6** is required.

0. clone this repo

```bash
$ git clone https://gitlab.com/pinle-go/nre hp-nre
$ cd hp-nre
```

1. install required packages

```bash
$ pip3 install -r requirements
```

2. get pretrained GloVe word embeddings (42B 300d Common Crawl version)

```bash
$ wget "http://nlp.stanford.edu/data/glove.42B.300d.zip"
$ unzip glove.42B.300d.zip
```

3. install nltk tokenize data

```bash
$ python3 -c "import nltk; nltk.download('punkt')"
```

4. reproduce the submmited prediction

```bash
$ python3 train.py --model PiecePoolConcat --lr 0.01 --epochs 66 --batch_size 32 --seed 1231245324 --infer --infer_fn inferred.txt
```

The prediction of test data is in "inferred.txt".

5. (optional) reproduce the model comparsion study

First fetch remote branch "compare", then switch to that branch.
Then play with "run_compare.sh"
```bash
$ git fetch 
$ git checkout origin/compare
$ cat run_compare.sh
```
