import torch as tc
from logzero import logger

from utils import DynamicLinear, Conv2dSame, load_embedings

n_label = 19
n_hidden = 32
n_channel = 16
n_pos = 21
emb_dim = 300
pos_emb_dim = 5
kernel_size = 3
dropout = 0.5


class Base(tc.nn.Module):
    def __init__(self, word_dict, device, use_pe=False):
        super().__init__()

        self.device = device
        self.use_pe = use_pe
        self.word_dict = word_dict

        self.embeds = tc.nn.Embedding(word_dict.n_word, emb_dim)
        if use_pe:
            self.pos_embeds = tc.nn.Embedding(n_pos, pos_emb_dim)

        self.mlp = tc.nn.Sequential(
            tc.nn.Dropout(dropout),
            DynamicLinear(n_hidden),
            tc.nn.ReLU(),
            tc.nn.Dropout(dropout),
            DynamicLinear(n_label),
        )

    def forward(self, xs):
        ys = []
        for word_indices, pos1, pos2, pos_indices in xs:
            word_indices = word_indices.to(self.device)
            word_vecs = self.embeds(word_indices)
            if self.use_pe:
                pos_indices = pos_indices.to(self.device)
                pos_vecs = self.pos_embeds(pos_indices)
                seq_len = len(word_indices)
                pos_vecs = pos_vecs.view(seq_len, -1)
                vecs = tc.cat([word_vecs, pos_vecs], dim=1)
            else:
                vecs = word_vecs

            seq_len = len(word_indices)
            vecs = vecs.view(1, 1, seq_len, -1)

            y = self.forward_agg(vecs, pos1, pos2)
            ys.append(y)

        ys = tc.cat(ys, dim=0)
        return self.mlp(ys)

    def load_embed(self, embed_fn):
        available_words = set(self.word_dict.word_to_index.keys())
        embed_dict = load_embedings(embed_fn, available_words)

        cnt = 0
        for word, index in self.word_dict.word_to_index.items():
            if word not in embed_dict:
                continue

            self.embeds.weight.data[index].copy_(
                tc.from_numpy(embed_dict[word])
            )
            cnt += 1
        logger.info(
            "Loaded pre-trained from %s, %d out of %d matched",
            embed_fn,
            cnt,
            self.word_dict.n_word,
        )

    def print_architecture(self):
        t = [0] * 10
        with tc.no_grad():
            fake_xs = [
                [
                    tc.LongTensor(t, device=self.device),
                    3,
                    7,
                    tc.LongTensor([t * 2], device=self.device),
                ]
            ]
            self.forward(fake_xs)
        logger.info(str(self))


class CNN(Base):
    def __init__(self, word_dict, device, use_pe=False):
        super().__init__(word_dict, device, use_pe=use_pe)

        self.convs = tc.nn.Sequential(
            tc.nn.Conv2d(1, n_channel, kernel_size, padding=1),
            tc.nn.ReLU(),
            tc.nn.MaxPool2d(3, stride=1, padding=1),
            tc.nn.Conv2d(n_channel, 1, kernel_size, padding=1),
        )

    def forward_agg(self, vecs, pos1, pos2):
        x = self.convs(vecs)
        x = x.max(dim=2)[0]
        return x


class PiecePool(CNN):
    def forward_agg(self, vecs, pos1, pos2):
        x = self.convs(vecs)
        x = tc.cat(
            [
                x[:, :, : pos1 + 1].max(dim=2, keepdim=False)[0],
                x[:, :, pos1 : pos2 + 1].max(dim=2, keepdim=False)[0],
                x[:, :, pos2:].max(dim=2, keepdim=False)[0],
            ],
            dim=1,
        )
        return x


class PiecePoolConcat(CNN):
    def __init__(self, word_dict, device, use_pe=False):
        super().__init__(word_dict, device, use_pe=use_pe)

        self.convs = tc.nn.Sequential(
            Conv2dSame(1, n_channel, kernel_size),
            tc.nn.ReLU(),
            Conv2dSame(n_channel, 1, kernel_size),
        )

    def forward_agg(self, vecs, pos1, pos2):
        x = self.convs(vecs)
        x = tc.cat(
            [
                x[:, :, : pos1 + 1].max(dim=2, keepdim=False)[0].flatten(),
                x[:, :, pos1 : pos2 + 1]
                .max(dim=2, keepdim=False)[0]
                .flatten(),
                x[:, :, pos2:].max(dim=2, keepdim=False)[0].flatten(),
                vecs[:, :, pos1].flatten(),
                vecs[:, :, pos2].flatten(),
            ],
            dim=0,
        )
        return x.view(1, -1)


class PieceConv(CNN):
    def forward_agg(self, vecs, pos1, pos2):
        xs = (
            vecs[:, :, : pos1 + 1],
            vecs[:, :, pos1 - 1 : pos2 + 1],
            vecs[:, :, pos2 - 1 :],
        )
        ys = [self.convs(x).mean(dim=2).flatten() for x in xs]

        return tc.cat(ys, dim=0).view(1, -1)


class RNN(Base):
    def __init__(self, word_dict, device, use_pe=False):
        super().__init__(word_dict, device, use_pe=use_pe)

        self.input_size = emb_dim + (pos_emb_dim if use_pe else 0)
        self.n_layer = 2
        self.n_direction = 2
        self.rnn = tc.nn.GRU(
            input_size=self.input_size,
            hidden_size=n_hidden,
            num_layers=self.n_layer,
            bidirectional=(self.n_direction == 2),
            batch_first=True,
            dropout=dropout,
        )

    def forward_agg(self, vecs, pos1, pos2):
        x = vecs.squeeze(1)
        _, x = self.rnn(x)
        x = x.view(1, -1)
        return x


class Attn(RNN):
    def forward_agg(self, vecs, pos1, pos2):
        seq_len = vecs.size(2)
        x = vecs.squeeze(1)
        hs, x = self.rnn(x)
        x = x.view(self.n_layer, self.n_direction, -1).mean(dim=0).view(-1, 1)
        hs = hs.view(seq_len, x.size(0))
        ws = hs.mm(x)
        return (ws * hs).mean(dim=0).view(1, -1)


class Concat(RNN):
    def forward_agg(self, vecs, pos1, pos2):
        seq_len = vecs.size(2)
        x = vecs.squeeze(1)
        hs, x = self.rnn(x)
        x = x.view(self.n_layer, self.n_direction, -1).mean(dim=0).flatten()
        hs = hs.view(seq_len, x.size(0))  # batch_size = 1
        return tc.cat([x, hs[pos1], hs[pos2]], dim=0).view(1, -1)
