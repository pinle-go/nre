import torch as tc


class DynamicLinear(tc.nn.Module):
    """Linear layer which can automatically infer the input size"""

    def __init__(self, out_features):
        super(DynamicLinear, self).__init__()

        self.register_parameter("weight", None)
        self.register_parameter("bias", None)
        self.in_features = -1
        self.out_features = out_features

    def reset_parameters(self, x):
        data = x.data.new(self.out_features, x.size(1))
        data = tc.nn.init.kaiming_normal_(data)
        self.weight = tc.nn.Parameter(data)
        self.bias = tc.nn.Parameter(x.data.new(self.out_features).zero_())
        self.in_features = x.size(1)

    def forward(self, x):
        x = x.view(x.size()[0], -1)

        if self.__getattr__("weight") is None:
            self.reset_parameters(x)

        return tc.nn.functional.linear(x, self.weight, bias=self.bias)

    def __repr__(self):
        return "{}(in_features={}, out_features={})".format(
            self.__class__.__name__, self.in_features, self.out_features
        )


class Conv2dSame(tc.nn.Module):
    """Conv2d layer whose output has the same shape as input"""

    def __init__(
        self,
        in_channels,
        out_channels,
        kernel_size,
        bias=True,
        padding_layer=tc.nn.ReflectionPad2d,
    ):
        super().__init__()
        ka = kernel_size // 2
        kb = ka - 1 if kernel_size % 2 == 0 else ka
        self.net = tc.nn.Sequential(
            padding_layer((ka, kb, ka, kb)),
            tc.nn.Conv2d(in_channels, out_channels, kernel_size, bias=bias),
        )

    def forward(self, x):
        return self.net(x)
