from itertools import chain

import numpy as np
import torch as tc
from nltk import word_tokenize
from torch.utils.data import Dataset, DataLoader

relation_to_index = {
    "other": 0,
    "message-topic(e1,e2)": 1,
    "message-topic(e2,e1)": 2,
    "product-producer(e1,e2)": 3,
    "product-producer(e2,e1)": 4,
    "instrument-agency(e1,e2)": 5,
    "instrument-agency(e2,e1)": 6,
    "entity-destination(e1,e2)": 7,
    "entity-destination(e2,e1)": 8,
    "cause-effect(e1,e2)": 9,
    "cause-effect(e2,e1)": 10,
    "component-whole(e1,e2)": 11,
    "component-whole(e2,e1)": 12,
    "entity-origin(e1,e2)": 13,
    "entity-origin(e2,e1)": 14,
    "member-collection(e1,e2)": 15,
    "member-collection(e2,e1)": 16,
    "content-container(e1,e2)": 17,
    "content-container(e2,e1)": 18,
}

index_to_relation = {
    0: "Other",
    1: "message-topic(e1,e2)",
    2: "message-topic(e2,e1)",
    3: "product-producer(e1,e2)",
    4: "product-producer(e2,e1)",
    5: "instrument-agency(e1,e2)",
    6: "instrument-agency(e2,e1)",
    7: "entity-destination(e1,e2)",
    8: "entity-destination(e2,e1)",
    9: "cause-effect(e1,e2)",
    10: "cause-effect(e2,e1)",
    11: "component-whole(e1,e2)",
    12: "component-whole(e2,e1)",
    13: "entity-origin(e1,e2)",
    14: "entity-origin(e2,e1)",
    15: "member-collection(e1,e2)",
    16: "member-collection(e2,e1)",
    17: "content-container(e1,e2)",
    18: "content-container(e2,e1)",
}


def load_embedings(fn, available_words):
    """load word vectors from fn

    fn should be a txt file and each line is like
        "word 0.1 0.2 0.3 0.5"
    """
    available_words = set(available_words)
    embed_dict = {}
    with open(fn) as f:
        for line in f:
            line = line.strip()
            if line == "":
                continue
            word, vec = line.split(maxsplit=1)
            if word not in available_words:
                continue
            vec = np.fromstring(vec, dtype=np.float32, sep=" ")
            embed_dict[word] = vec
    return embed_dict


def parse_sentence(sentence):
    """Extract tokens and entities indices from raw sentence"""
    underscore = "___"
    words = (
        sentence.replace("</e1>", "")
        .replace("</e2>", "")
        .replace("<e1>", " " + underscore)
        .replace("<e2>", " " + underscore)
    )
    words = word_tokenize(words)
    e1_index, e2_index = -1, -1
    for i, word in enumerate(words):
        if words[i][: len(underscore)] == underscore:
            if e1_index == -1:
                e1_index = i
            else:
                e2_index = i
            words[i] = words[i][len(underscore) :]
    if e1_index == -1 or e2_index == -1:
        raise Exception("Wrong: " + sentence)
    return words, e1_index, e2_index


def load_datafile(fn, is_test):
    """Load SemEval2018 dataset"""
    data = []
    with open(fn) as f:
        lines = f.readlines()

    if is_test:
        for line in lines:
            data_id, sentence = line.strip().split(maxsplit=1)
            extracted_words, e1_index, e2_index = parse_sentence(sentence)
            data.append((extracted_words, e1_index, e2_index, int(data_id)))
    else:
        for i in range(len(lines) // 4):
            sentence = lines[i * 4].split("\t", 1)[1].strip()
            relation = lines[i * 4 + 1].strip()

            label = relation_to_index[relation.lower()]
            extracted_words, e1_index, e2_index = parse_sentence(sentence)

            data.append((extracted_words, e1_index, e2_index, label))

    words = set(chain.from_iterable([x[0] for x in data]))
    word_dict = WordDict(words)
    data = [
        ([word_dict.index(word) for word in x[0]], x[1], x[2], x[3])
        for x in data
    ]

    return data, word_dict


class WordDict:
    """This class stores the mapping between words and indices."""

    unk = "<unk>"

    def __init__(self, words):
        self.word_to_index = {WordDict.unk: 0}
        self.index_to_word = {0: WordDict.unk}

        for word in sorted(set(words)):
            index = len(self.word_to_index)

            self.word_to_index[word] = index
            self.index_to_word[index] = word

    def index(self, word):
        return self.word_to_index.get(word, 0)

    def word(self, index):
        return self.index_to_word.get(index, WordDict.unk)

    @property
    def n_word(self):
        return len(self.word_to_index)


class NREDataset(Dataset):
    """PyTorch compatible Dataset for NRE dataset"""

    @staticmethod
    def split(dset, ratio, seed=0):
        """split one dataset into two datasets"""
        np.random.seed(seed)

        data, word_dict = dset.data, dset.word_dict
        indices = set(
            [i for i in range(len(data)) if np.random.random() < ratio]
        )

        train_data = [data[i] for i in range(len(data)) if i in indices]
        valid_data = [data[i] for i in range(len(data)) if i not in indices]

        return (
            NREDataset(data_dict=(train_data, word_dict)),
            NREDataset(data_dict=(valid_data, word_dict)),
        )

    @staticmethod
    def get_train_valid_dev(dset, seed=0):
        """split one dataset into three datasets 70/10/20"""
        train_dset, valid_dev_dset = NREDataset._split(dset, 0.7, seed=seed)
        valid_dset, dev_dset = NREDataset.split(
            valid_dev_dset, 2 / 3.0, seed=seed
        )
        return train_dset, valid_dset, dev_dset

    def __init__(self, data_filename=None, data_dict=None, is_test=False):
        super().__init__()
        if data_dict is not None:
            data, word_dict = data_dict
        else:
            data, word_dict = load_datafile(data_filename, is_test)
        self.data = data
        self.word_dict = word_dict

        self.word_indices = [x[0] for x in data]
        self.pos1s = [x[1] for x in data]
        self.pos2s = [x[2] for x in data]
        self.pos_indices = [_compute_pe(x[0], x[1], x[2]) for x in data]
        self.labels = [x[3] for x in data]

        self._dls = {}

    def __getitem__(self, index):
        return (
            self.word_indices[index],
            self.pos1s[index],
            self.pos2s[index],
            self.pos_indices[index],
            self.labels[index],
        )

    def __len__(self):
        return len(self.word_indices)

    def dataloader(self, batch_size, shuffle=False):
        if (batch_size, shuffle) not in self._dls:

            def collate_fn(batch):
                xs, ys = [], []
                for word_indices, pos1, pos2, pos_indices, y in batch:
                    word_indices = tc.LongTensor(word_indices)
                    pos_indices = tc.LongTensor(pos_indices)
                    xs.append((word_indices, pos1, pos2, pos_indices))
                    ys.append(y)
                ys = tc.LongTensor(ys)

                return xs, ys

            self._dls[batch_size, shuffle] = DataLoader(
                self,
                batch_size=batch_size,
                shuffle=shuffle,
                collate_fn=collate_fn,
            )

        return self._dls[batch_size, shuffle]


def _clip(item, v_min, v_max):
    if item > v_max:
        return v_max
    if item < v_min:
        return v_min
    return item


def _compute_pe(words, pos1, pos2):
    result = []
    for i in range(len(words)):
        rel1 = _clip(i - pos1, -10, 10) // 2 + 5
        rel2 = _clip(i - pos2, -10, 10) // 2 + 5
        result.append([rel1, rel2])

    return result
