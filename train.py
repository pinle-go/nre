import argparse

import torch as tc
from logzero import logger

import net
from utils import evaluate, NREDataset, index_to_relation


parser = argparse.ArgumentParser()
parser.add_argument("--model", dest="model", default="PieceConv")
parser.add_argument("--lr", type=float, default=1e-2)
parser.add_argument("--seed", type=int, default=0)
parser.add_argument("--epochs", type=int, default=30)
parser.add_argument("--batch_size", type=int, default=32)
parser.add_argument("--infer", action="store_true")
parser.add_argument("--infer_fn", type=str, default="inferred.txt")
args = parser.parse_args()

device = tc.device("cuda" if tc.cuda.is_available() else "cpu")

embed_fn = (
    "/drive2/hanpeng/word2vecs/glove.42B.300d.txt"
)  # "empty_word2vec.txt"
dset = NREDataset("data/SemEval2010_task8_training/TRAIN_FILE.TXT")
if args.infer:
    train_dset, valid_dset = NREDataset.split(dset, 0.9)
    infer_dset = NREDataset(
        "data/SemEval2010_task8_testing/TEST_FILE.txt", is_test=True
    )
else:
    train_dset, valid_dset, dev_dset = NREDataset.get_train_valid_dev(
        dset, args.seed
    )

tc.manual_seed(args.seed)
if tc.cuda.is_available:
    tc.cuda.manual_seed(args.seed)

model = getattr(net, args.model)(train_dset.word_dict, device, use_pe=True)
model.load_embed(embed_fn)
model.to(device)
model.print_architecture()

lr = args.lr
criterion = tc.nn.CrossEntropyLoss()
optimizer = tc.optim.Adamax(model.parameters(), lr=lr)


def valid(dset):
    model.eval()

    y_true, y_pred = [], []
    dl = dset.dataloader(args.batch_size)
    with tc.no_grad():
        for xs, ys in dl:
            ys = ys.to(device)
            ys_pred = model(xs).argmax(dim=1)

            y_true += [index_to_relation[x] for x in ys.flatten().tolist()]
            y_pred += [
                index_to_relation[x] for x in ys_pred.flatten().tolist()
            ]

    p, r, f1 = evaluate(y_true, y_pred)
    return p, r, f1


def train(dset):
    model.train()

    dl = dset.dataloader(args.batch_size, shuffle=True)
    total_correct, total_loss, cnt = 0, 0, 0
    for xs, ys in dl:
        ys = ys.to(device)
        optimizer.zero_grad()

        loss = 0
        ys_pred = model(xs)
        loss += criterion(ys_pred, ys)
        loss.backward()
        optimizer.step()

        total_correct += (ys_pred.argmax(dim=1) == ys).to(dtype=tc.float).sum()
        total_loss += loss.item() * len(xs)
        cnt += len(xs)

    return total_loss / cnt, total_correct / cnt


def main():
    logger.info("   | TrLoss | TrAccu | Valid " + " " * 12 + "| Dev")
    info_template = (
        "{:2d} | {:.4f} | {:2.2f}  "
        "| {:1.3f} {:1.3f} {:1.3f} "
        "| {:1.3f} {:1.3f} {:1.3f}"
    )

    for epoch in range(args.epochs):
        if epoch % 30 == 0 and epoch != 0:
            global optimizer, lr
            lr = lr / 10.0
            optimizer = tc.optim.Adamax(model.parameters(), lr=lr)
        train_loss, train_acc = train(train_dset)
        valid_prf1 = valid(valid_dset)
        if args.infer:
            dev_prf1 = (0, 0, 0)
        else:
            dev_prf1 = valid(dev_dset)
        logger.info(
            info_template.format(
                epoch, train_loss, train_acc * 100, *valid_prf1, *dev_prf1
            )
        )


def infer(fn):
    logger.info("starting infer")
    model.eval()
    dl = infer_dset.dataloader(args.batch_size)

    f = open(fn, "w")
    with tc.no_grad():
        for xs, idxs in dl:
            ys = model(xs).argmax(dim=1).cpu().tolist()
            for i, y in zip(idxs.flatten().tolist(), ys):
                f.write("{} {}\n".format(i, index_to_relation[y]))
    f.close()
    logger.info("finish infer, labels are written to %s", fn)


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        logger.info("Training is manually stoped!")
        pass
    if args.infer:
        infer(args.infer_fn)
